from django.db import models

# Create your models here.
class Product(models.Model):
    name = models.CharField(max_length=45)
    description = models.TextField()
    code = models.CharField(max_length=20)
    bannner = models.ImageField(null= True, upload_to='products')
    quantity = models.PositiveIntegerField()

    def __str__(self):
        return self.code + ' - ' + self.name